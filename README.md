Service Windows de cloture et mise en paiement des fiches visiteurs de GSB ( BTS SIO SLAM)

Fonction avec une base de données en ligne: freedb (mdp et lien dans la fiche de situation)

Pour télécharger l'application finale qui travaille avec une bdd en ligne, voir https://adopplereffect.wordpress.com/ppe/

- récupérer l'archive zip sur mon portfolio https://adopplereffect.wordpress.com/ppe/ OU:
- récupérer le répository grâce à un git clone dans le répertoire de votre choix: 
	git clone https://adoppi@bitbucket.org/adoppi/servicegsb.git
- copier le dossier décompressé dans "C:\Temp" OU
	a partir du projet cloné: 
	ServiceGestionClotureGSB\bin\Release\ServiceGestionClotureGSB.exe (! ne pas confondre avec le fichier .exe.config)
	!!! ET ServiceGestionClotureGSB\bin\Release\MySql.Data.dll !!!
	Dans c:\Temp\ServiceGestionClotureGSB (dossier a créer)
- intallation du service: Ouvrir une invite de commande en ADMINISTRATEUR
	Entrer: "C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe" "C:\Temp\ServiceGestionClotureGSB.NET\ServiceGestionClotureGSB.exe"
	Chercher le service dans : Panneau de Configuration → Système et Sécurité → Outils d'administration → Services
	Le démarrer et le mettre en automatique 
-désinstaller le service dans invite de commande admin: sc delete "ServiceGestionClotureGSB.NET"

Application crée par Auriane DOPPLER