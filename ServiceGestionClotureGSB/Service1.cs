﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Timers;

namespace ServiceGestionClotureGSB
{
    /// <summary>
    /// Classe principale éxécutée du service.
    /// Contient les methodes permettant l'exécution du service et d'un timer <see cref="OnStart(string[])"/> <see cref="OnStop()"/>,
    /// la connection à la bdd et l'éxécution d'une requête dans un évènement éxécuté à chaque intervale du timer <see cref="OnElapsedTime(object, ElapsedEventArgs)"/>
    /// et la création et l'écriture d'un log <see cref="WriteLog(string, bool)"/>
    /// </summary>
    public partial class Service1 : ServiceBase
    {
        //initialisation du timer
        Timer Timer = new Timer();
        int Interval = 10000; // 10000 ms = 10 secondes

        //initialisation des identifiants pour la connexion à la base de données (ici en ligne)
        private static string serveur = "freedb.tech";
        private static string port = "3306";
        private static string basedonnees = "freedbtech_gsbfrais";
        private static string utilisateur = "freedbtech_userGsb";
        private static string mdp = "secret";
        /*
        // identifiants pour utilisation en local (si la bdd existe):
        private static string serveur = "localhost";
        private static string port = "3306";
        private static string basedonnees = "gsb_frais";
        private static string utilisateur = "userGsb";
        private static string mdp = "secret";
        */

        //Bloc éxécuté, lance le timer
        /// <summary>
        /// Bloc principal, éxécuté
        /// Lance l'application
        /// </summary>
        public Service1()
        {
            InitializeComponent();
            this.ServiceName = "ServiceGestionClotureGSB.NET";
        }

        /// <summary>
        /// Méthode qui s'éxécute au démarrage du service
        /// démarre le timer et appelle l'évènement <see cref="OnElapsedTime(object, ElapsedEventArgs)"/>
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            WriteLog("Le service à été démarré\n");
            Timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            Timer.Interval = Interval;
            Timer.Enabled = true;
        }

        // S'éxécute à chaque intervale du timer. Evenement qui appelle les methodes de connexion à la bdd, et qui modifie l'état des fiches en fonction du jour du mois
        /// <summary>
        /// Evenement qui appelle les methodes de connection à la bdd <see cref="AccesBdd.AccesBdd(string)"/>, et vérifie qu'elle s'est bien effectuée <see cref="AccesBdd.GetSuccesConnexion()"/>
        /// vérifie si la connection à bien été faire <see cref="AccesBdd.GetSuccesConnexion()"/>
        /// modifie l'état des fiches avec une requete update <see cref="AccesBdd.Requete(string)"/> en fonction du jour <see cref="GestionDates.Entre(int, int)"/> 
        /// et ferme la connextion pour ne pas surcharger <see cref="AccesBdd.FermerConnexion()"/>
        /// </summary>
        /// <remarks> S'éxécute à chaque intervale du timer.</remarks>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            //Connexion à la bdd de gsb et création du curseur:
            AccesBdd connexionGsb = new AccesBdd("SERVER=" + serveur + "; port=" + port + "; DATABASE=" + basedonnees + "; UID=" + utilisateur + "; PASSWORD=" + mdp);

            //Exécute la requête uniquement si la connexion a bien été effectuée
            if (connexionGsb.GetSuccesConnexion())
            {
                //Entre 1er et 10 du mois lors de la campagne de validation, on dois cloturer (CL) toutes les fiches crées (CR) le mois précédent
                if (GestionDates.Entre(1, 10))
                {
                    //On récupère le mois précédent et son année
                    string mois = GestionDates.GetMoisPrecedent();
                    string dateModif = GestionDates.GetDateModif();
                    WriteLog("Fiches validées pour le mois: " + mois + "\n");
                    //Mise a jour des fiches crées (CR) vers l'état cloturé (CL)
                    connexionGsb.Requete("update fichefrais set idetat='CL', datemodif='" + dateModif + "' where mois =" + mois + " and idetat='CR'");
                }
                //A partir du 20 du mois on souhaite mettre en remboursement (RB) toutes les fiches validées (VA) du mois precedent
                else if (GestionDates.Entre(20, 31))
                {
                    //Récupération du mois précédent l'actuel (au format yyyymm)
                    string mois = GestionDates.GetMoisPrecedent();
                    string dateModif = GestionDates.GetDateModif();
                    WriteLog("Fiches mises en remboursement pour le mois: " + mois + "\n");
                    //Mise a jour des fiches validées (VA) vers l'état remboursé (RB)
                    connexionGsb.Requete("update fichefrais set idetat='RB', datemodif='" + dateModif + "' where mois = " + mois + " and idetat='VA'");
                }
                //ferme la connexion à la base de données entre chaque intervale du timer
                connexionGsb.FermerConnexion();
            }
            else
            {
               WriteLog("La maj des fiches n'a pas pu être opérée. Assurez vous d'être bien connecté.e au réseau et que la bdd gsb_frais exite\n");
            }

        }

        /// <summary>
        /// S'éxécute lors de l'arrêt du service, stoppe le timer et log l'arrêt.
        /// </summary>
        protected override void OnStop()
        {
            Timer.Stop();
            WriteLog("Le service à été arrêté.\n");
        }


        /// <summary>
        /// Permet de créer un fichier log (.txt) si il n'existe pas déjà
        /// Inscrit un message entré en paramètre ainsi que l'heure si souhaité.
        /// </summary>
        /// <param name="logMessage">message à entrer dans le log</param>
        /// <param name="addTimeStamp">indique si on veut mettre l'heure (true par défaut)</param>
        private void WriteLog(string logMessage, bool addTimeStamp = true)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var filePath = String.Format("{0}\\{1}_{2}.txt",
                path,
                ServiceName,
                DateTime.Now.ToString("yyyyMMdd", CultureInfo.CurrentCulture)
                );

            if (addTimeStamp)
                logMessage = String.Format("[{0}] - {1}",
                    DateTime.Now.ToString("HH:mm:ss", CultureInfo.CurrentCulture),
                    logMessage);

            File.AppendAllText(filePath, logMessage);
        }
    }
}
